import unittest

from .. import config
from ..emailservice import SesEmailService, Email


class EmailTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._ses = SesEmailService(config.AWS_ACCESS_KEY_ID,
                                   config.AWS_ACCESS_KEY_SECRET)

    def test_send_valid_raw_email(self):
        email = Email(
            From="seb@meetosper.com",
            To="seb@meetosper.com",
            Subject="Blah"
        )
        email.set_body("pja'pjo'pjo")
        email.set_attachement("test/test_files/test_file.txt", "blah.txt")
        email.finish()
        res = self._ses.send_email(email)
        self.assertIn("SendRawEmailResponse", res)

    def test_send_invalid_raw_email(self):
        with self.assertRaises(Exception):
            email = Email(
                From="seb@meetosper.com",
                To="seb@meetosper.com",
                Subject="Blah"
            )
            email.set_attachement("test/test_files/test_fake", "blah.txt")
            email.finish()
            res = self._ses.send_email(email)

import unittest

from ..pdfier import Pdfier


class PdfTest(unittest.TestCase):

    def test_invalid_html_pdfication(self):
        with self.assertRaises(Exception):
            test_file = "test/test_pdfs/test.pdf"
            Pdfier.pdfy(
                html_filename="test/test_pdfs/test_html.html",
                css_filename="test/test_pdfs/test_css.css",
                to=test_file
            )

    def test_valid_html_pdfication(self):
        test_file = "test/test_pdfs/test.pdf"
        Pdfier.pdfy(
            html_filename="templates/pdf.tpl",
            css_filename="templates/style.css",
            to=test_file
        )
        self.assertIsNotNone(open(test_file))

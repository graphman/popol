import unittest

from ..db import PopolDB


class DBTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.db = PopolDB()

    def setUp(self):
        self.db.prepare()

    def tearDown(self):
        self.db.finish()

    def test_simple_valid_query(self):
        res = self.db.execute("SELECT * FROM transaction")
        self.assertIsNotNone(res)

    def test_simple_invalid_query(self):
        with self.assertRaises(Exception):
            res = self.db.execute("BLAH * FROM troll")

import hashlib

from functools import wraps
import popol.config as config


def not_implemented(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        raise NotImplementedError()
    return wrapper


def code_from(name, date):
    return str(
        hashlib.sha256(
            ("%s-%s|%s" % (name, str(date),
                           config.SECRET_KEY))).hexdigest())[0:4]


def int_list(tuple_list):
    try:
        return [i[0] for i in tuple_list]
    except Exception:
        return []

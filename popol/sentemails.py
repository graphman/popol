from popol.reporter import db
from popol.models.sent_report_emails import SentReportEmail


class SentReportEmails(object):

    def set_sent(self, report, content, to, when):
        email = SentReportEmail(account_id=report.account_id,
                                report_id=report.report_id,
                                sent=when, email_content=content,
                                email_address=to)

        db.add(email)
        print email

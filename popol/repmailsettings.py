from popol.db import PopolDB


class ReportEmailSettings(object):
    def __init__(self):
        self.db = PopolDB()

    def get_last_settings(self, account_id):
        res = self.db.execute(""" SELECT * FROM reportemailsettings
            WHERE account_id = :account_id
            ORDER BY created DESC LIMIT 1
        """, {"account_id": account_id})
        return res

    def set_settings(self, account_id, frequency, receive_report=True):
        res = self.db.execute(""" INSERT INTO reportemailsettings
            (account_id, receive_report, report_frequency, created)
            VALUES (:account_id, :receive, :frequency, now())
        """, {
            "account_id": account_id,
            "receive": receive_report,
            "frequency": frequency
        })
        return res

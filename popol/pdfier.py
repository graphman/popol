from weasyprint import HTML, CSS


class Pdfier(object):

    @staticmethod
    def pdfy(html_string="<html> </html>",
             css_string=".body{}", html_filename=None,
             css_filename=None, to="blah.pdf"):

        if html_filename:
            html = HTML(filename=html_filename)
        else:
            try:
                html = HTML(string=html_string)
            except:
                raise Exception("Error loading html string")

        if css_filename:
            css = CSS(filename=css_filename)
        else:
            try:
                css = CSS(string=css_string)
            except Exception:
                raise Exception("Error loading css string")

        html.write_pdf(to, stylesheets=[css])

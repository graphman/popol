import os
import config

from jinja2 import Environment, FileSystemLoader


templates_path = os.path.join(config.WORKING_DIR, "popol/templates")


def datetimeformat(value, format='%d-%m-%Y'):
    return value.strftime(format)

env = Environment(loader=FileSystemLoader(templates_path))

env.filters["datetimeformat"] = datetimeformat

DATA_TYPES = ["svg", "png", "text", "value"]


class StatsTemplate(object):

    def __init__(self, template_name="pdf.tpl"):
        self.data = {}
        try:
            self.template = env.get_template(template_name)
        except Exception as e:
            print template_name, "isn't a valid template file !", e

    def add_data(self, name, data):
        self.data[name] = data

    def remove_data(self, data_name):
        if data_name in self.data:
            del self.data[data_name]

    def render_to_file(self, filename):
        with open(
            os.path.join(filename), "w"
        ) as f:
            f.write(self.template.render(data=self.data))


class Data(object):

    def __init__(self, data_type, **kwargs):
        if not data_type in DATA_TYPES:
            raise Exception(data_type, "isn't a valid data_type !")
        self.type = data_type
        self.args = kwargs
        print self.args
        for key, value in self.args.items():
            setattr(self, key, value)

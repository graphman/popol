import os

from datetime import datetime

from bnkpdatabase.models import SimpleKyc

from popol.config import (
    OUTPUT_DIR,
    DEFAULT_EMAIL_OPTIONS,
    AWS_ACCESS_KEY_ID,
    AWS_ACCESS_KEY_SECRET
)
from popol.db import PopolDB
from popol.utils import code_from
from popol.stpl import Data
from popol.pdfier import Pdfier
from popol.emailservice import SesEmailService, Email
from popol.models.report import ReportModel

db = PopolDB()

from popol.sentemails import SentReportEmails
emails = SentReportEmails()


class Report(object):

    def __init__(self, card, from_date, to_date):
        self.card_id = card.card_id
        self.account_id = card.user_id
        self.user_info = self.get_user_info()
        self.data = {}
        self.html_file = None
        self.pdf_file = None
        self.created = datetime.now()
        self.from_date = from_date
        self.to_date = to_date
        self.ses = SesEmailService(AWS_ACCESS_KEY_ID, AWS_ACCESS_KEY_SECRET)
        self.report = None

    def generate_html(self, tpl, output=OUTPUT_DIR):
        try:
            self.html_file = self.get_filename("html", dir_name=output)
            tpl.add_data("account", Data("value", account=self.user_info))
            tpl.add_data("report", Data("value", report=self))
            tpl.render_to_file(self.html_file)
            return True
        except Exception as e:
            print e
            return False

    def generate_pdf(self, output=OUTPUT_DIR,
                     css_filename="popol/templates/pdf_base.css"):
        try:
            self.pdf_file = self.get_filename("pdf", dir_name=output)
            Pdfier.pdfy(html_filename=self.html_file,
                        css_filename=css_filename,
                        to=self.pdf_file)
            self.clean()
            self.insert()
        except Exception as e:
            print e

    def send_to(self, to, options=DEFAULT_EMAIL_OPTIONS):
        #CONDITIONS : TABLE SETTINGS
        self.email = Email(From=options["From"], To=to, Subject="Blah")

        content = options["body"]
        self.email.set_body(content)

        self.email.set_attachement(self.pdf_file, options["filename"])
        self.email.finish()
        res = self.ses.send_email(self.email)
        if "SendRawEmailResponse" in res:
            emails.set_sent(self, content, to, datetime.now())
        return res

    def get_filename(self, ext, dir_name=OUTPUT_DIR):
        filename = "%(first_name)s-%(date)s-%(code)s.%(ext)s"
        first_name = self.user_info.first_name
        date = datetime.now().date()
        code = code_from(first_name, date)
        filename = filename % {"first_name": first_name, "date": date,
                               "code": code, "ext": ext}
        if dir_name:
            return os.path.join(dir_name, filename)
        return filename

    def clean(self):
        #os.remove(self.pdf_file)
        #os.remove(self.html_file)
        pass

    def get_user_info(self):
        res = db.query(SimpleKyc).filter_by(account_id=self.account_id).first()
        return res

    def insert(self):
        rep = ReportModel(card_id=self.card_id, created=datetime.now(),
                          report_filename=self.pdf_file,
                          start_date=self.from_date,
                          end_date=self.to_date)
        db.add(rep)
        self.report_id = rep.report_id

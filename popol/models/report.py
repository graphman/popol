from sqlalchemy import (
    BIGINT,
    Column,
    ForeignKey,
    TIMESTAMP,
    VARCHAR
)
from bnkpdatabase.models import DeclarativeBase, Card


class ReportModel(DeclarativeBase):
    __tablename__ = "report"
    __table_args__ = {}

    report_id = Column(BIGINT, primary_key=True)
    created = Column(TIMESTAMP)
    card_id = Column(VARCHAR, ForeignKey("card.card_id"), nullable=False)
    report_filename = Column(VARCHAR)
    start_date = Column(TIMESTAMP)
    end_date = Column(TIMESTAMP)

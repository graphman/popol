from sqlalchemy import (
    BIGINT,
    BOOLEAN,
    Column,
    ForeignKey,
    TEXT,
    TIMESTAMP,
    VARCHAR
)
from bnkpdatabase.models import DeclarativeBase, Card


class SentReportEmail(DeclarativeBase):
    __tablename__ = "sentreportemails"
    __table_args__ = {}

    account_id = Column(BIGINT,
                        ForeignKey("account.account_id"), primary_key=True)
    report_id = Column(BIGINT,
                       ForeignKey("report.report_id"), primary_key=True)
    sent = Column(TIMESTAMP)
    email_content = Column(TEXT)
    email_address = Column(VARCHAR)

from sqlalchemy import (
    BIGINT,
    BOOLEAN,
    Column,
    ForeignKey,
    TIMESTAMP,
    VARCHAR
)
from sqlalchemy.dialects.postgresql import ENUM
from bnkpdatabase.models import DeclarativeBase, Card


class AccountReportEmailSettings(DeclarativeBase):
    __tablename__ = "reportemailsettings"
    __table_args__ = {}

    account_id = Column(BIGINT,
                        ForeignKey("account.account_id"), primary_key=True)
    receive_report = Column(BOOLEAN, default=True)
    report_frequency = Column(
        ENUM("WEEKLY", "MONTHLY", name="report_frequency"),
        default="MONTHLY"
    )
    created = Column(TIMESTAMP)

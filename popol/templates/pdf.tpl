{% from 'utils.tpl' import render_data %}
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <title>Osper report</title>
    <script>
      function(d,s,id){
      var js,fjs=d.getElementsByTagName(s)[0];
      var p=/^http:/.test(d.location)?'http':'https';
      if(!d.getElementById(id)){
      js=d.createElement(s);
      js.id=id;
      js.src=p+'://platform.twitter.com/widgets.js';
      fjs.parentNode.insertBefore(js,fjs);
      }}(document, 'script', 'twitter-wjs');
    </script>
  </head>
  <body>
    <table id="main-container">
      <thead>
	<tr>
	  <th id="report-infos">
	    <div id="report-name">
	      {{ data.account.account.first_name }}'
	      {% if data.account.account.first_name[-1] != "s" %}s
	      {% endif%} osper report
	    </div>
	    <div id="report-range">
	      From {{ data.report.report.from_date|datetimeformat("%d %B %Y") }}
	      to {{ data.report.report.to_date|datetimeformat("%d %B %Y") }}
	    </div>
	    <hr/>
	    <div id="report-date">
	      {{ data.report.report.created|datetimeformat("%d %B %Y") }}
	    </div>
	  </th>
	  <th id="osper-infos">
	    <div id="osper-logo">
	      <embed src="../popol/templates/img/osper.svg" type="image/svg+xml"/>
	    </div>
            <div id="osper-links">
              <center>
		<a href="http://meetosper.com">meetosper.com</a> &#8226; <a href="mailto:hello@meetosper.com">hello@meetosper.com</a>
              </center>
	    </div>
	  </th>
	</tr>
      </thead>
      <tbody>
	<br/><br/>
	<tr>
	  <td colspan="2" id="welcome">
	    <h3>Dear {{ data.account.account.first_name }},</h3>
            Below you will find your Osper report. This is a month by month
	    summary of how much you have received and spent over the past
	    six months. <br>
	    {% if data.savings.savings > 0 %}
	    Congratulations on saving
	    <span class="osper-colored">
	      &pound;{{ data.savings.savings }}
	    </span>
	    this month, put this towards your savings goals.
	    {% endif %}
	  </td>
	</tr>
	<tr>
	  <td colspan="2" id="graph-in-out">
	    <h4>Money In/Out in the last six months</h4>
	    {{ render_data(data.in_out) }}
	  </td>
	</tr>
	<br/><br/>
	<tr>
	  <td id="call-to-action">
	    <h4 class="top">Call to action</h4>
            <div>What are you saving up for ? Share it with us by tweeting with
              <a href="https://twitter.com/intent/tweet?button_hashtag=osperwishlist&text=I'm%20saving%20for" class="twitter-hashtag-button" data-size="large" data-related="meetosper" data-dnt="true">
		#osperwishlist.
              </a>
	      <br/><br/>
	      <strong>Tip :</strong> Savings goals are helping our members to save more and buy the things they want sooner, <a href="https://twitter.com/search?q=osperwishlist&mode=realtime"> click here to see some examples.</a>
	    </div>
	  </td>
	  <td id="top-purchases">
	    <h4 class="top">Top 3 purchases</h4>
            <table id="top-purchases-table">
              <thead>
		<tr>
                  <th></th>
                  <th>Description</th>
                  <th>Date</th>
                  <th class="bordered-right">Amount</th>
		</tr>
              </thead>
              <tbody>
		{% for purchase in data.purchases.purchases %}
		  <tr>
                    <td>{{ loop.index }}</td>
                    <td>{{ purchase.description[0:15] }}</td>
                    <td>{{ purchase.time|datetimeformat("%d %b %Y") }}</td>
                    <td class="bordered-right">
		      <span class="pound">
			&pound;
		      </span>
		      {{- ("%0.2f" % (-purchase.amount/100)) -}}
		    </td>
		  </tr>
		{% endfor %}
              </tbody>
            </table>
	  </td>
	</tr>
      </tbody>
      <br/><br/><br/><br/><br/>
      <tfoot>
	<tr>
	  <td colspan="2" id="footer">
	    <center>
	      <a href="mailto:hello@meetosper.com">
		hello@meetosper.com
	      </a>
	      &#8226; &copy; 2013 Osper Ltd. &#8226; Company no. 07958759
	      <br/>
	      <div id="questions">
		Any questions or feedback ?
		<a href="mailto:damola@meetosper.com">
		  damola@meetosper.com
		</a>
	      </div>
	    </center>
	  </td>
	</tr>
      </tfoot>
    </table>
  </body>
</html>

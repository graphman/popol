{% macro render_data(data) %}
    {% if data.type == "svg" %}
        <embed src="{{ data.args.src }}" type="image/svg+xml">
    {% elif data.type == "png" %}
        <img src="{{ data.args.src }}" alt="{{ data.args.alt }}" height="{{ data.args.height }}" width="{{ data.args.width }}" type="image/png">
    {% elif data.type == "text" %}
        <p class="{{ data.args.classes }}">{{ data.args.content }}</p>
    {% endif %}
{% endmacro %}
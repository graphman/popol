from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from bnkpdatabase.models import DeclarativeBase, Card

from popol.utils import not_implemented
from popol import config

from models import report, report_email_settings, sent_report_emails


class SQLQuerySender(object):

    @not_implemented
    def execute(self, sql, args):
        pass

    def execute_sqlfile(self, filename, args):
        with open(filename, "r") as f:
            return self.execute(f.read(), args)


class PopolDB(SQLQuerySender):

    Session = sessionmaker(expire_on_commit=False)

    def __init__(self, database_uri=config.SQLALCHEMY_DATABASE_URI):
        self.database_uri = database_uri
        self.engine = create_engine(self.database_uri)

        metadata = DeclarativeBase.metadata
        metadata.bind = self.engine
        metadata.create_all()

        self.Session.configure(bind=self.engine)
        self.session = scoped_session(self.Session)

    def prepare(self):
        self.session()

    def finish(self):
        self.session.commit()
        self.session.remove()

    def execute(self, sql, args={}):
        res = self.session.execute(sql, args)
        self.finish()
        return res

    def add(self, obj, commit=True):
        self.session.add(obj)
        if commit:
            self.session.commit()

    def query(self, *args, **kwargs):
        return self.session.query(*args, **kwargs)

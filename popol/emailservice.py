import boto

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

from popol.utils import not_implemented


class EmailService(object):

    @not_implemented
    def send_email(self, from_address, to_address, subject, body):
        pass

    @not_implemented
    def send_email_object(self, email):
        pass


class SesEmailService(EmailService):
    def __init__(self, access_key_id, access_key_secret):
        self.connection = boto.connect_ses(
            aws_access_key_id=access_key_id,
            aws_secret_access_key=access_key_secret
        )

    def send_email(self, email):
        res = self.connection.send_raw_email(
            email.msg.as_string(),
            source=email.get("From"),
            destinations=email.get("To")
        )
        return res


class Email(object):

    def __init__(self, **kwargs):
        self.msg = MIMEMultipart()
        self._update(kwargs)
        self.body = None
        self.attachement = None

    def _update(self, kwargs):
        for key, value in kwargs.items():
            self.msg[key] = value

    def set(self, key, value):
        self.msg[key] = value

    def set_body(self, body):
        self.body = MIMEText(body)

    def get(self, key):
        return self.msg.get(key, None)

    def set_attachement(self, filepath, filename):
        file_content = open(filepath, "r").read()
        self.attachement = MIMEApplication(file_content)
        self.attachement.add_header("Content-Disposition",
                                    "attachement",
                                    filename=filename)

    def finish(self):
        if self.body:
            self.msg.attach(self.body)
        if self.attachement:
            self.msg.attach(self.attachement)

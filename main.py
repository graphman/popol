from pygal import Bar, Config

from bnkpdatabase.models import Transaction, SavingsTransactions, Card

from sqlalchemy.sql.expression import extract

from popol.config import CUSTOM_STYLE
from popol.stpl import StatsTemplate, Data
from popol.reporter import Report, db
from popol.utils import int_list

from datetime import datetime
from dateutil.relativedelta import relativedelta


def get_top_purchases(card_id, month, year):

    top_purchases = db.query(Transaction).filter_by(
        card_id=card_id,
        transaction_type="DEBIT").filter(
        extract("month", Transaction.time) == month,
        extract("year", Transaction.time) == year
        ).order_by(Transaction.amount).limit(3).all()
    return top_purchases


def get_in_out(card_id):

    data_debit = []
    data_credit = []
    months = []
    now = datetime.now()

    for i in xrange(6):
        delta = relativedelta(months=(6-(i)))
        now_new = now - delta
        debit_amounts = db.query(-(Transaction.amount/100)).filter_by(
            transaction_type="DEBIT", card_id=card_id).filter(
            extract("month", Transaction.time) == now_new.month,
            extract("year", Transaction.time) == now_new.year
            ).all()

        #debit_amounts is a list of tuple
        debit = sum(int_list(debit_amounts))

        credit_amounts = db.query(Transaction.amount/100).filter_by(
            transaction_type="CREDIT", card_id=card_id).filter(
            extract("month", Transaction.time) == now_new.month,
            extract("year", Transaction.time) == now_new.year
            ).all()

        #credit_amounts is a list of tuple
        credit = sum(int_list(credit_amounts))

        data_debit.append(debit)
        data_credit.append(credit)
        months.append(now_new.strftime("%b %y"))

    # data are got from now to the past ==> reverse to get data ordered
    #data_debit.reverse()
    #data_credit.reverse()
    #months.reverse()

    return {
        "in": data_credit,
        "out": data_debit,
        "months": months
    }


def get_savings_by_month(account_id, month, year=datetime.now().year):
    savings_blah = db.query(SavingsTransactions.amount/100).filter_by(
        account_id=account_id).filter(
        SavingsTransactions.amount > 0,
        extract("month", SavingsTransactions.created) == month,
        extract("year", SavingsTransactions.created) == year).all()
    return sum(int_list(savings_blah))


def InOutBarChart(title="Sample Title", style=None, data={}):
    config = Config(fill=False)
    config.css.append("popol/templates/pygal_custom_css.css")

    pie = Bar(config)
    pie.title = title
    pie.style = style

    pie.x_title = "Month"

    #\u00A3 is pound sign
    pie.y_title = "Amount (%s)" % u"\u00A3"

    pie.width = 500
    pie.height = 450

    pie.rounded_bars = 2
    pie.human_readable = True

    pie.print_values = True
    pie.x_labels = data["months"]

    pie.add("In", data["in"])

    pie.add("Out", data["out"])
    return pie


if __name__ == '__main__':

    cards = db.query(Card).filter_by(status="ALLOCATED").all()

    for card in cards:
        end_date = datetime.now().date()
        start_date = (datetime.now() - relativedelta(days=5)).date()

        report = Report(card, start_date, end_date)

        #get top_purchases for the last month
        last_month = datetime.now() - relativedelta(months=1)

        top_purchases = get_top_purchases(report.card_id,
                                          last_month.month, last_month.year)
        in_out = get_in_out(report.card_id)

        bar = InOutBarChart(title="", style=CUSTOM_STYLE, data=in_out)
        bar.render_to_file(report.get_filename("svg"))

        tpl = StatsTemplate(template_name="pdf.tpl")
        tpl.add_data("purchases", Data("value", purchases=top_purchases))
        tpl.add_data(
            "in_out",
            Data("svg", src=report.get_filename("svg"))
        )

        savings = get_savings_by_month(report.account_id, end_date.month - 1,
                                       end_date.year)

        tpl.add_data("savings", Data("value", savings=savings))

        report.generate_html(tpl)
        report.generate_pdf(css_filename="popol/templates/style.css")
        report.send_to("seb@meetosper.com")
        break
